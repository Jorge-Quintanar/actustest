﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebApp.Models;
using WebApp.Controllers;
using System.Security.Cryptography.X509Certificates;
using System.Linq;

namespace ProductionDynamic.Tests
{

    [TestClass]
    public class Tests
    {
        string DBName = "Actus";

        int plaza = 5; //Hermosillo 
        //int plaza = 27; //Tijuana

        int semana = 9;

        [TestMethod]
        public void MinProductionZero()
        {
            var Controller = new Controller();

            List<Schedule> ScheduleList = Controller.GetSchedules(DBName, plaza, semana);

            var counter = 0;

            // 5 'Lunes - Viernes'
            // 6 'Sábado'
            // 7 'Domingo'
            foreach (var ScheduleRow in ScheduleList)
            {
                if (ScheduleRow.DayType == 5 && ScheduleRow.Taste == "Clásico")
                {
                    if (ScheduleRow.MinimumDisplay == 0)
                    {
                        counter++;
                    }
                }
            }

            Assert.IsTrue(counter == 0, counter + " Tiendas tienen producción minima en cero.");
        }

        [TestMethod]
        public void OpeningCoffeDisplay()
        {
            var Controller = new Controller();

            List<Schedule> ScheduleList = Controller.GetSchedules(DBName, plaza, semana);

            var counter = 0;

            // 5 'Lunes - Viernes'
            // 6 'Sábado'
            // 7 'Domingo'
            foreach (var ScheduleRow in ScheduleList)
            {
                var autoStartHour = ScheduleRow.Opening;

                if (ScheduleRow.DayType == 5 && ScheduleRow.Taste == "Clásico")
                {
                    List<double> HoursList = new List<double>
                    {
                        ScheduleRow.H0,
                        ScheduleRow.H1,
                        ScheduleRow.H2,
                        ScheduleRow.H3,
                        ScheduleRow.H4,
                        ScheduleRow.H5,
                        ScheduleRow.H6,
                        ScheduleRow.H7,
                        ScheduleRow.H8,
                        ScheduleRow.H9,
                        ScheduleRow.H10,
                        ScheduleRow.H11,
                        ScheduleRow.H12,
                        ScheduleRow.H13,
                        ScheduleRow.H14,
                        ScheduleRow.H15,
                        ScheduleRow.H16,
                        ScheduleRow.H17,
                        ScheduleRow.H18,
                        ScheduleRow.H19,
                        ScheduleRow.H20,
                        ScheduleRow.H21,
                        ScheduleRow.H22,
                        ScheduleRow.H23
                    };

                    if (HoursList[autoStartHour] == 0)
                    {
                        counter++;
                    }
                }
            }
            Assert.IsTrue(counter == 0, counter + " Tiendas no piden en su hora de apertura");
        }

        //[TestMethod]
        //public void VarietiesCoffeDisplay()
        //{
        //    var Controller = new Controller();

        //    List<Schedule> ScheduleList = Controller.GetSchedules(DBName, plaza, semana);

        //    var counter = 0;

        //    List<string> Tiendas = new List<string> { };
        //    List<string> TiendasNoOpeningCoffe = new List<string> { };
        //    List<string> TiendasSuperGestionNotAppear = new List<string> { };


        //    foreach (var ScheduleRow in ScheduleList)
        //    {
        //        var autoStartHour = ScheduleRow.Opening;
                
        //        //Forman parte del listado de tiendas supergestión?
        //        #region
        //        if (
        //                ScheduleRow.CR == "50ZDP"
        //                || ScheduleRow.CR == "503IH"
        //                || ScheduleRow.CR == "50KXK"
        //                || ScheduleRow.CR == "50WQF"
        //                || ScheduleRow.CR == "50ABR"
        //                || ScheduleRow.CR == "50FUO"
        //                || ScheduleRow.CR == "50DNL"
        //                || ScheduleRow.CR == "50TFC"
        //                || ScheduleRow.CR == "50OTO"
        //                || ScheduleRow.CR == "50EZH"
        //                || ScheduleRow.CR == "50NKT"
        //                || ScheduleRow.CR == "50EKF"
        //                || ScheduleRow.CR == "50RI6"
        //                || ScheduleRow.CR == "50GSB"
        //                || ScheduleRow.CR == "50YNV"
        //                || ScheduleRow.CR == "50NEJ"
        //                || ScheduleRow.CR == "50KUK"
        //                || ScheduleRow.CR == "50QWC"
        //                || ScheduleRow.CR == "508BA"
        //                || ScheduleRow.CR == "50ALI"
        //                || ScheduleRow.CR == "50YES"
        //                || ScheduleRow.CR == "50S2I"
        //                || ScheduleRow.CR == "50RUZ"
        //                || ScheduleRow.CR == "50FPB"
        //                || ScheduleRow.CR == "50JSN"
        //                || ScheduleRow.CR == "50KVT"
        //                || ScheduleRow.CR == "50AKO"
        //                || ScheduleRow.CR == "50XKA"
        //                || ScheduleRow.CR == "50WID"
        //                || ScheduleRow.CR == "50TCH"
        //                || ScheduleRow.CR == "50CWV"
        //                || ScheduleRow.CR == "50TCZ"
        //                || ScheduleRow.CR == "50COS"
        //                || ScheduleRow.CR == "50N9C"
        //                || ScheduleRow.CR == "50NRQ"
        //                || ScheduleRow.CR == "50XCK"
        //                || ScheduleRow.CR == "50NCA"
        //                || ScheduleRow.CR == "50ZBT"
        //                || ScheduleRow.CR == "50GEG"
        //                || ScheduleRow.CR == "50DZA"
        //                || ScheduleRow.CR == "50HXW"
        //                || ScheduleRow.CR == "50TDL"
        //                || ScheduleRow.CR == "50EZI"
        //                || ScheduleRow.CR == "50KDI"
        //                || ScheduleRow.CR == "50DZN"
        //                || ScheduleRow.CR == "50RHB"
        //                || ScheduleRow.CR == "50HNG"
        //                || ScheduleRow.CR == "50VBW"
        //                || ScheduleRow.CR == "50SZL"
        //                || ScheduleRow.CR == "50FOX"
        //                || ScheduleRow.CR == "50XRY"
        //                || ScheduleRow.CR == "50EPN"
        //                || ScheduleRow.CR == "50LCF"
        //                || ScheduleRow.CR == "50FMZ"
        //                || ScheduleRow.CR == "50FXT"
        //                || ScheduleRow.CR == "50VPN"
        //                || ScheduleRow.CR == "50YKG"
        //                || ScheduleRow.CR == "50XJJ"
        //                || ScheduleRow.CR == "50OQY"
        //                || ScheduleRow.CR == "50UER"
        //                || ScheduleRow.CR == "50HDO"
        //                || ScheduleRow.CR == "50XZW"
        //                || ScheduleRow.CR == "50ITN"
        //                || ScheduleRow.CR == "50VWY"
        //                || ScheduleRow.CR == "50YZU"
        //                || ScheduleRow.CR == "50UZU"
        //                || ScheduleRow.CR == "50XPI"
        //                || ScheduleRow.CR == "500AE"
        //                || ScheduleRow.CR == "50OJD"
        //                || ScheduleRow.CR == "50TUJ"
        //                || ScheduleRow.CR == "500TV"
        //                || ScheduleRow.CR == "50XDL"
        //                || ScheduleRow.CR == "50LHZ"
        //                || ScheduleRow.CR == "50LLU"
        //                || ScheduleRow.CR == "50ZXS"
        //                || ScheduleRow.CR == "50GYI"
        //                || ScheduleRow.CR == "50NRM"
        //                || ScheduleRow.CR == "50R9E"
        //                || ScheduleRow.CR == "50AFZ"
        //                || ScheduleRow.CR == "50FVU"
        //                || ScheduleRow.CR == "50UJR"
        //                || ScheduleRow.CR == "50QWO"
        //                || ScheduleRow.CR == "50ZXD"
        //                || ScheduleRow.CR == "50KUX"
        //                || ScheduleRow.CR == "50JWG"
        //                || ScheduleRow.CR == "50DJO"
        //                || ScheduleRow.CR == "50AWV"
        //                || ScheduleRow.CR == "502Y0"
        //                || ScheduleRow.CR == "50ZVK"
        //                || ScheduleRow.CR == "50ZVD"
        //                || ScheduleRow.CR == "50ZVJ"
        //                || ScheduleRow.CR == "50YHV"
        //                || ScheduleRow.CR == "50NIX"
        //                || ScheduleRow.CR == "50NTJ"
        //                || ScheduleRow.CR == "50OQT"
        //                || ScheduleRow.CR == "50JXM"
        //                || ScheduleRow.CR == "5035K"
        //                || ScheduleRow.CR == "50VVL"
        //                || ScheduleRow.CR == "50OXK"
        //                || ScheduleRow.CR == "50OKV"
        //                || ScheduleRow.CR == "50OYU"
        //                || ScheduleRow.CR == "50ATV"
        //                || ScheduleRow.CR == "50VBZ"
        //                || ScheduleRow.CR == "50ZDD"
        //                || ScheduleRow.CR == "50ZYY"
        //                || ScheduleRow.CR == "50CQF"
        //                || ScheduleRow.CR == "50YWT"
        //                || ScheduleRow.CR == "50FYV"
        //                || ScheduleRow.CR == "50BGH"
        //                || ScheduleRow.CR == "50EVA"
        //                || ScheduleRow.CR == "50KHO"
        //                || ScheduleRow.CR == "50WPE"
        //                || ScheduleRow.CR == "50DOE"
        //                || ScheduleRow.CR == "50YCD"
        //                || ScheduleRow.CR == "50CFZ"
        //                || ScheduleRow.CR == "50WIC"
        //                || ScheduleRow.CR == "50XEI"
        //                || ScheduleRow.CR == "50CGA"
        //                || ScheduleRow.CR == "50JCQ"
        //                || ScheduleRow.CR == "50OCB"
        //                || ScheduleRow.CR == "50XKF"
        //                || ScheduleRow.CR == "50UYD"
        //                || ScheduleRow.CR == "50ZXF"
        //                || ScheduleRow.CR == "50GHH"
        //                || ScheduleRow.CR == "50OUL"
        //                || ScheduleRow.CR == "50THI"
        //                || ScheduleRow.CR == "50XMA"
        //                || ScheduleRow.CR == "50CYT"
        //                || ScheduleRow.CR == "50RON"
        //                || ScheduleRow.CR == "50XEE"
        //                || ScheduleRow.CR == "50TTP"
        //                || ScheduleRow.CR == "50OWQ"
        //                || ScheduleRow.CR == "50JGQ"
        //                || ScheduleRow.CR == "50GJL"
        //                || ScheduleRow.CR == "50WFP"
        //                || ScheduleRow.CR == "50KFY"
        //                || ScheduleRow.CR == "50ODR"
        //                || ScheduleRow.CR == "50QJU"
        //                || ScheduleRow.CR == "50NCL"
        //                || ScheduleRow.CR == "50ZBD"
        //                || ScheduleRow.CR == "50EKB"
        //                || ScheduleRow.CR == "50HNB"
        //                || ScheduleRow.CR == "50QTV"
        //                || ScheduleRow.CR == "50EGG"
        //                || ScheduleRow.CR == "50ERR"
        //                || ScheduleRow.CR == "50EZR"
        //                || ScheduleRow.CR == "50JGK"
        //                || ScheduleRow.CR == "50ZWW"
        //                || ScheduleRow.CR == "50HZM"
        //                || ScheduleRow.CR == "50TGR"
        //                || ScheduleRow.CR == "50GHN"
        //                || ScheduleRow.CR == "50JUP"
        //                || ScheduleRow.CR == "50WIG"
        //                || ScheduleRow.CR == "50WIM"
        //                || ScheduleRow.CR == "50IXP"
        //                || ScheduleRow.CR == "50INL"
        //                || ScheduleRow.CR == "50JWY"
        //                || ScheduleRow.CR == "50RJS"
        //                || ScheduleRow.CR == "50ZCK"
        //                || ScheduleRow.CR == "50XSA"
        //                || ScheduleRow.CR == "50XSW"
        //                || ScheduleRow.CR == "50HHH"
        //                || ScheduleRow.CR == "50BWJ"
        //                || ScheduleRow.CR == "50NHY"
        //                || ScheduleRow.CR == "50LSC"
        //                || ScheduleRow.CR == "50LMW"
        //                || ScheduleRow.CR == "50SHH"
        //                || ScheduleRow.CR == "50MPF"
        //                || ScheduleRow.CR == "50CQW"
        //                || ScheduleRow.CR == "50WVE"
        //                || ScheduleRow.CR == "50KXE"
        //                || ScheduleRow.CR == "50MQQ"
        //                || ScheduleRow.CR == "50WPF"
        //                || ScheduleRow.CR == "50SMK"
        //                || ScheduleRow.CR == "50MNK"
        //                || ScheduleRow.CR == "50MIS"
        //                || ScheduleRow.CR == "50PMD"
        //                || ScheduleRow.CR == "50JHF"
        //                || ScheduleRow.CR == "50NCK"
        //                || ScheduleRow.CR == "50NLV"
        //                || ScheduleRow.CR == "50ZVH"
        //                || ScheduleRow.CR == "50AMP"
        //                || ScheduleRow.CR == "50ZVU"
        //                || ScheduleRow.CR == "50EYN"
        //                || ScheduleRow.CR == "50JPW"
        //                || ScheduleRow.CR == "50UUJ"
        //                || ScheduleRow.CR == "50NQP"
        //                || ScheduleRow.CR == "50FPF"
        //                || ScheduleRow.CR == "50PJH"
        //                || ScheduleRow.CR == "50OQL"
        //                || ScheduleRow.CR == "50PLN"
        //                || ScheduleRow.CR == "50POR"
        //                || ScheduleRow.CR == "50IAP"
        //                || ScheduleRow.CR == "50POJ"
        //                || ScheduleRow.CR == "50ZBV"
        //                || ScheduleRow.CR == "50BAP"
        //                || ScheduleRow.CR == "50QUN"
        //                || ScheduleRow.CR == "50OUO"
        //                || ScheduleRow.CR == "50SKV"
        //                || ScheduleRow.CR == "50RXW"
        //                || ScheduleRow.CR == "50GGY"
        //                || ScheduleRow.CR == "50SKK"
        //                || ScheduleRow.CR == "50BVD"
        //                || ScheduleRow.CR == "50QSH"
        //                || ScheduleRow.CR == "50JOW"
        //                || ScheduleRow.CR == "50QFZ"
        //                || ScheduleRow.CR == "50NFV"
        //                || ScheduleRow.CR == "50TEN"
        //                || ScheduleRow.CR == "50TTR"
        //                || ScheduleRow.CR == "50NDQ"
        //                || ScheduleRow.CR == "50URA"
        //                || ScheduleRow.CR == "50DOR"
        //                || ScheduleRow.CR == "50VDN"
        //                || ScheduleRow.CR == "50OVE"
        //                || ScheduleRow.CR == "50VRX"
        //                || ScheduleRow.CR == "50VNA"
        //                || ScheduleRow.CR == "50EYI"
        //                || ScheduleRow.CR == "50JXN"
        //                || ScheduleRow.CR == "50WZE"
        //                || ScheduleRow.CR == "50JIE"
        //                || ScheduleRow.CR == "50TCC"
        //                || ScheduleRow.CR == "50ZXU"
        //                || ScheduleRow.CR == "50PED"
        //                || ScheduleRow.CR == "50GYE"
        //                || ScheduleRow.CR == "50PLY"
        //                || ScheduleRow.CR == "50QXI"
        //                || ScheduleRow.CR == "50TKO"
        //                || ScheduleRow.CR == "50WXN"
        //                || ScheduleRow.CR == "50FGP"
        //                || ScheduleRow.CR == "50VXA"
        //                || ScheduleRow.CR == "50ZRY"
        //                || ScheduleRow.CR == "50RUY"
        //                || ScheduleRow.CR == "50YKJ"
        //                || ScheduleRow.CR == "50JWF"
        //                || ScheduleRow.CR == "50WQD"
        //                || ScheduleRow.CR == "50EZZ"
        //                || ScheduleRow.CR == "50YPX"
        //                || ScheduleRow.CR == "50WIQ"
        //                || ScheduleRow.CR == "50GGW"
        //                || ScheduleRow.CR == "50UFF"
        //                || ScheduleRow.CR == "50SKT"
        //                || ScheduleRow.CR == "50QXX"
        //                || ScheduleRow.CR == "5085K"
        //                || ScheduleRow.CR == "50NXN"
        //                || ScheduleRow.CR == "50LWW"
        //                || ScheduleRow.CR == "50XKY"
        //                || ScheduleRow.CR == "50CCP"
        //                || ScheduleRow.CR == "50NHS"
        //                || ScheduleRow.CR == "50LBU"
        //                || ScheduleRow.CR == "50EKA"
        //                || ScheduleRow.CR == "50TBQ"
        //                || ScheduleRow.CR == "50BJX"
        //                || ScheduleRow.CR == "50A9C"
        //                || ScheduleRow.CR == "50FRT")
        //        #endregion
        //        {
        //            if (ScheduleRow.DayType == 5 && ScheduleRow.Taste == "Intenso")
        //            {
        //                List<double> HoursList = new List<double>
        //                {
        //                    ScheduleRow.H0,
        //                    ScheduleRow.H1,
        //                    ScheduleRow.H2,
        //                    ScheduleRow.H3,
        //                    ScheduleRow.H4,
        //                    ScheduleRow.H5,
        //                    ScheduleRow.H6,
        //                    ScheduleRow.H7,
        //                    ScheduleRow.H8,
        //                    ScheduleRow.H9,
        //                    ScheduleRow.H10,
        //                    ScheduleRow.H11,
        //                    ScheduleRow.H12,
        //                    ScheduleRow.H13,
        //                    ScheduleRow.H14,
        //                    ScheduleRow.H15,
        //                    ScheduleRow.H16,
        //                    ScheduleRow.H17,
        //                    ScheduleRow.H18,
        //                    ScheduleRow.H19,
        //                    ScheduleRow.H20,
        //                    ScheduleRow.H21,
        //                    ScheduleRow.H22,
        //                    ScheduleRow.H23
        //                };
                   
        //                    //Piden café en su apertura?
        //                    if (HoursList[autoStartHour] != 0)  
        //                    {
        //                           Tiendas.Add(ScheduleRow.LocationName);
        //                           counter++;
        //                    }
        //                    else
        //                    {
        //                        TiendasNoOpeningCoffe.Add(ScheduleRow.LocationName);
        //                    }

        //                    //if (ScheduleRow.MinimumDisplay != 0)
        //                    //{
        //                    //    TiendasSuperGestionNotAppear.Add(ScheduleRow.LocationName); 
        //                    //}

        //            }
        //            //La tienda Forma parte del listado pero no pide café intenso entre semana 
        //        }
        //    }
        //    NoOpeningCoffeList(Tiendas);

        //    //Assert.IsTrue(counter == 254, counter + " Piden café en la apertura en variedades.");
        //    Assert.IsTrue(counter == 254, counter + " Tiendas del listado piden café en la apertura en variedades");
        //}

        [TestMethod]
        public void Opening24Hours()
        {
            var Controller = new Controller();
            List<Schedule> ScheduleList = Controller.GetSchedules(DBName, plaza, semana);

            var counter = 0;
            List<string> Tiendas = new List<string> { };

            foreach (var ScheduleRow in ScheduleList)
            {
                var startHour = ScheduleRow.WeekDayStartHour;
                List<double> HoursList = new List<double>
                            {
                                ScheduleRow.H0,
                                ScheduleRow.H1,
                                ScheduleRow.H2,
                                ScheduleRow.H3,
                                ScheduleRow.H4,
                                ScheduleRow.H5,
                                ScheduleRow.H6,
                                ScheduleRow.H7,
                                ScheduleRow.H8,
                                ScheduleRow.H9,
                                ScheduleRow.H10,
                                ScheduleRow.H11,
                                ScheduleRow.H12,
                                ScheduleRow.H13,
                                ScheduleRow.H14,
                                ScheduleRow.H15,
                                ScheduleRow.H16,
                                ScheduleRow.H17,
                                ScheduleRow.H18,
                                ScheduleRow.H19,
                                ScheduleRow.H20,
                                ScheduleRow.H21,
                                ScheduleRow.H22,
                                ScheduleRow.H23
                            };
                
                if (startHour == 0)//Es 24 horas.
                {
                    //Hermosillo
                    if (plaza == 5)
                    {
                        if (ScheduleRow.DayType == 5 && ScheduleRow.Taste == "Clásico")
                        {
                            if (HoursList[6] == 0)
                            {
                                Tiendas.Add(ScheduleRow.LocationName);
                                counter++;
                            }
                        }
                    }

                    //Tijuana
                    if (plaza == 27)
                    {
                        if (ScheduleRow.DayType == 5 && ScheduleRow.Taste == "Clásico")
                        {
                            if (HoursList[5] == 0)
                            {
                                Tiendas.Add(ScheduleRow.LocationName);
                                counter++;
                            }
                        }
                    }
                }
            }
            NoOpeningCoffeList(Tiendas);

            Assert.IsTrue(counter == 0, counter + " Tiendas 24 horas no piden café al inicio de la gestión.");
        }

        public List<string> NoOpeningCoffeList(List<string> LocationsList)
        {
            Console.WriteLine(LocationsList);
            return LocationsList;
        }

}
}
