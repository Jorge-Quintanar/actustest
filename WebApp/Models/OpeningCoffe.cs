﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class OpeningCoffe
    {
        public int Id { get; set; }
        public string Week { get; set; }
        public string Asesor { get; set; }
        public int District { get; set; }
        public string Location { get; set; }
        public int Openinghour { get; set; }
        public int Monday { get; set; }
        public int Tuesday { get; set; }
        public int Wednesday { get; set; }
        public int Thursday { get; set; }
        public int Friday { get; set; }
        public int Saturday { get; set; }
        public int Sunday { get; set; }

        public OpeningCoffe()
        {

        }
        public OpeningCoffe(int id, string week, string asesor, int district, string location, int openinghour, int monday, int tuesday, int wednesday, int thursday, int friday, int saturday, int sunday)
        {

            Id = id;
            Week = week;
            Asesor = asesor;
            District = district;
            Location = location;
            Openinghour = openinghour;
            Monday = monday;
            Monday = monday;
            Tuesday = tuesday;
            Wednesday = wednesday;
            Thursday = thursday;
            Friday = friday;
            Saturday = saturday;
            Sunday = sunday;
        }
    }
}
