﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace WebApp.Models
{
   public class DisponibilityWeekly
    {
        public string Plaza { set; get; }
        public int Week { set; get; }
        public string Asesor { set; get; }
        public int District { set; get; }
        public string Cr { set; get; }
        public string Tienda { set; get; }
        public string Taste { set; get; }
        public int MinimalDisplay { set; get; }
        public int RealDisplay { set; get; }
        public int Production { set; get; }
        public int DisplayWeekday { set; get; }
        public int DisplaySaturday { set; get; }
        public int DisplaySunday { set; get; }
        public double WeightedDisponibility { set; get; }
        public int OnlineAverage { set; get; }
        public string Comments { set; get; }
        public int NeedDisplayWeekday { set; get; }
        public int NeedDisplaySaturday { set; get; }
        public int NeedDisplaySunday { set; get; }



        public DisponibilityWeekly()
        {

        }
        public DisponibilityWeekly(int plaza,int week, string asesor,int district, string cr,string tienda,string taste ,int minimalDisplay, int realDisplay, int production, 
            int displayWeekDay, int displaySaturday, int displaySunday, double weightedDisponibility,int onlineAverage, int comments, int needDisplayWeekday,
            int needDisplaySaturday, int needDisplaySunday)
        {

        }
    }
}
