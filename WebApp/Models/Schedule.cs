﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace WebApp.Models
{
   public class Schedule
    {
        public string CR { set; get; }
        public string LocationName { set; get; }
        public int Opening { set; get; }
        public int District { set; get; }
        public string Asesor { set; get; }
        public double H0 { set; get; }
        public double H1 { set; get; }
        public double H2 { set; get; }
        public double H3 { set; get; }
        public double H4 { set; get; }
        public double H5 { set; get; }
        public double H6 { set; get; }
        public double H7 { set; get; }
        public double H8 { set; get; }
        public double H9 { set; get; }
        public double H10 { set; get; }
        public double H11 { set; get; }
        public double H12 { set; get; }
        public double H13 { set; get; }
        public double H14 { set; get; }
        public double H15 { set; get; }
        public double H16 { set; get; }
        public double H17 { set; get; }
        public double H18 { set; get; }
        public double H19 { set; get; }
        public double H20 { set; get; }
        public double H21 { set; get; }
        public double H22 { set; get; }
        public double H23 { set; get; }
        public string Taste { set; get; }
        public int DayType { set; get; }
        public string DayName { set; get; }
        public int MinimumDisplay { set; get; }
        public int WeekDayStartHour { set; get; }
        public int WeekDayEndHour { set; get; }



        public Schedule()
        {

        }
    }
}
