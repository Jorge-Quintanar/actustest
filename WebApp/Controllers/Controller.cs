﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebApp.Models;
using WebApp.Tools;

namespace WebApp.Controllers
{
    public class Controller
    {

        [HttpGet]
        public List<DisponibilityWeekly> GetDisponibility(string name)
        {
            //if (Session["userid"] == null)
            //{
            //    return RedirectToAction("logout", "account");
            //}
            SqlCommand sqlCommand = new SqlCommand();
            //sqlCommand.CommandText =
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandText = "dashboard_GetHistoricalDisplayWeekly";

            DataTableReader reader = DatabaseConnection.ExecuteQuery(sqlCommand, name);

            var DisponibilityWeeklyList = new List<DisponibilityWeekly>();
            DisponibilityWeekly DisponibilityWeekly;
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    DisponibilityWeekly = new DisponibilityWeekly();
                    DisponibilityWeekly.Plaza = reader["Plaza"].ToString();
                    DisponibilityWeekly.Week = Convert.ToInt32(reader["Week"]);
                    DisponibilityWeekly.Asesor = reader["Asesor"].ToString();
                    DisponibilityWeekly.Tienda = reader["Tienda"].ToString();
                    DisponibilityWeekly.Taste = reader["Sabor"].ToString();
                    DisponibilityWeekly.MinimalDisplay = Convert.ToInt32(reader["ExhibicionMinimaOz"]);
                    DisponibilityWeekly.RealDisplay = Convert.ToInt32(reader["ExhibicionReal"]);
                    DisponibilityWeekly.Production = Convert.ToInt32(reader["ProduccionOz"]);
                    DisponibilityWeekly.DisplayWeekday = Convert.ToInt32(reader["DisponibilidadEntreSemana"]);
                    DisponibilityWeekly.DisplaySaturday = Convert.ToInt32(reader["DisponibilidadSabado"]);
                    DisponibilityWeekly.DisplaySunday = Convert.ToInt32(reader["DisponibilidadDomingo"]);
                    DisponibilityWeekly.WeightedDisponibility = Convert.ToDouble(reader["DisponibilidadPonderada"]);
                    DisponibilityWeekly.OnlineAverage = Convert.ToInt32(reader["OnlineAvg"]);
                    DisponibilityWeekly.Comments = reader["Comments"].ToString();

                    DisponibilityWeekly.NeedDisplayWeekday = 1;
                    if (reader["NeedDisplayWeekday"].ToString() == "No")
                    {
                        DisponibilityWeekly.NeedDisplayWeekday = 0;
                    }

                    DisponibilityWeekly.NeedDisplaySaturday = 1;
                    if (reader["NeedDisplaySaturday"].ToString() == "No")
                    {
                        DisponibilityWeekly.NeedDisplaySaturday = 0;
                    }

                    DisponibilityWeekly.NeedDisplaySunday = 1;
                    if (reader["NeedDisplaySunday"].ToString() == "No")
                    {
                        DisponibilityWeekly.NeedDisplaySunday = 0;
                    }
                 
                    DisponibilityWeeklyList.Add(DisponibilityWeekly);
                }
                reader.Read();
            }

            reader.Close();
            reader.Dispose();

            sqlCommand.Connection.Close();
            sqlCommand.Dispose();


            return DisponibilityWeeklyList;
        }

        [HttpGet]
        public List<Schedule> GetSchedules(string dbName, int Plaza, int week)
        {
            //if (Session["userid"] == null)
            //{
            //    return RedirectToAction("logout", "account");
            //}
            

            SqlCommand sqlCommand = new SqlCommand();
            //sqlCommand.CommandText =
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandText = "Test_GetMinimumCoffeDisplay";
            sqlCommand.Parameters.AddWithValue("@PlazaId", Plaza );
            sqlCommand.Parameters.AddWithValue("@Month", Convert.ToInt32(DateTime.Now.Month));
            sqlCommand.Parameters.AddWithValue("@WeekNumber", week);
            sqlCommand.Parameters.AddWithValue("@Year", Convert.ToInt32(DateTime.Now.Year));
            //sqlCommand.Parameters.AddWithValue("@Year", 2020);


            DataTableReader reader = DatabaseConnection.ExecuteQuery(sqlCommand, dbName);

            var SchedulesList = new List<Schedule>();
            Schedule Schedule;
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Schedule = new Schedule();
                    Schedule.CR = reader["CR"].ToString();
                    Schedule.LocationName = reader["Tienda"].ToString();
                    Schedule.Opening = Convert.ToInt32(reader["Apertura"]);
                    //Schedule.District = Convert.ToInt32(reader["Distrito"]);
                    Schedule.Asesor = reader["Asesor"].ToString();
                    Schedule.H0 = Convert.ToDouble(reader["H0"]);
                    Schedule.H1 = Convert.ToDouble(reader["H1"]);
                    Schedule.H2 = Convert.ToDouble(reader["H2"]);
                    Schedule.H3 = Convert.ToDouble(reader["H3"]);
                    Schedule.H4 = Convert.ToDouble(reader["H4"]);
                    Schedule.H5 = Convert.ToDouble(reader["H5"]);
                    Schedule.H6 = Convert.ToDouble(reader["H6"]);
                    Schedule.H7 = Convert.ToDouble(reader["H7"]);
                    Schedule.H8 = Convert.ToDouble(reader["H8"]);
                    Schedule.H9 = Convert.ToDouble(reader["H9"]);
                    Schedule.H10 = Convert.ToDouble(reader["H10"]);
                    Schedule.H11 = Convert.ToDouble(reader["H11"]);
                    Schedule.H12 = Convert.ToDouble(reader["H12"]);
                    Schedule.H13 = Convert.ToDouble(reader["H13"]);
                    Schedule.H14 = Convert.ToDouble(reader["H14"]);
                    Schedule.H15 = Convert.ToDouble(reader["H15"]);
                    Schedule.H16 = Convert.ToDouble(reader["H16"]);
                    Schedule.H17 = Convert.ToDouble(reader["H17"]);
                    Schedule.H18 = Convert.ToDouble(reader["H18"]);
                    Schedule.H19 = Convert.ToDouble(reader["H19"]);
                    Schedule.H20 = Convert.ToDouble(reader["H20"]);
                    Schedule.H21 = Convert.ToDouble(reader["H21"]);
                    Schedule.H22 = Convert.ToDouble(reader["H22"]);
                    Schedule.H23 = Convert.ToDouble(reader["H23"]);
                    Schedule.Taste = reader["Sabor"].ToString();
                    Schedule.DayType = Convert.ToInt32(reader["DayType"]);
                    Schedule.DayName = reader["DayName"].ToString();
                    Schedule.MinimumDisplay = Convert.ToInt32(reader["MinimumDisplay"]);
                    Schedule.WeekDayStartHour = Convert.ToInt32(reader["WeekDayStartHour"]);
                    Schedule.WeekDayEndHour = Convert.ToInt32(reader["WeekDayEndHour"]);

                    SchedulesList.Add(Schedule);
                }
                reader.Read();
            }

            reader.Close();
            reader.Dispose();

            sqlCommand.Connection.Close();
            sqlCommand.Dispose();


            return SchedulesList;
        }
    }
}
