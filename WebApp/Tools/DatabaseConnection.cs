﻿using System;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace WebApp.Tools
{
    /// <summary>
    /// Provides access to SQL Server database.
    /// </summary>
    public class DatabaseConnection
    {
        #region "Static Methods"

        //public static string GetConnectionString()
        //{
        //    //string serverName = ConfigurationManager.AppSettings["ServerName"];
        //    //string databaseName = ConfigurationManager.AppSettings["DatabaseName"];
        //    //string databaseUser = ConfigurationManager.AppSettings["DatabaseUser"];
        //    //string databasePassword = ConfigurationManager.AppSettings["DatabasePassword"];

        //    //var connectionString = "Data Source=" + serverName + ";User ID=" + databaseUser + ";Password=" + databasePassword + ";Initial Catalog=" + databaseName;
        //    //return connectionString;
        //    return "Data Source=" + "52.25.106.114" + ";User ID=" + "IMPUser" + ";Password=" + "ElectricIMP.2015" + ";Initial Catalog=" + "ActusBI_Oxxo";

        //}

        public static string GetConnectionString(string dbName)
        {
                return "Data Source=" + "52.25.106.114" + ";User ID=" + "IMPUser" + ";Password=" + "ElectricIMP.2015" + ";Initial Catalog=" + dbName;
        }

        public static int ExecuteNonQuery(SqlCommand sqlCommand, string dbName)
        {
            int rowsAffected = 0;

            using (SqlConnection sqlConnection = new SqlConnection(GetConnectionString(dbName)))
            {
                sqlConnection.Open();
                sqlCommand.Connection = sqlConnection;
                try
                {
                    rowsAffected = sqlCommand.ExecuteNonQuery();
                }
                catch (SqlException exception)
                {
                    sqlConnection.Close();
                    sqlCommand.Dispose();
                    throw exception;
                }
                catch (Exception exception)
                {
                    sqlConnection.Close();
                    sqlCommand.Dispose();
                    throw exception;
                }

                sqlConnection.Close();
                sqlCommand.Dispose();

            }

            return rowsAffected;
        }

        /// <summary>
        /// Executes a SQL statement with result like a Select command.
        /// </summary>
        /// <param name="sqlCommand">SQL command to be executed.</param>
        /// <returns>Returns a DataTableReader object with the data.</returns>

        public static DataTableReader ExecuteQuery(SqlCommand sqlCommand, string dbName)
        {

            DataSet dataSet = null;

            using (SqlConnection sqlConnection = new SqlConnection(GetConnectionString(dbName)))
            {
                sqlConnection.Open();
                sqlCommand.Connection = sqlConnection;

                SqlDataAdapter sqlAdapter = new SqlDataAdapter();
                sqlAdapter.SelectCommand = sqlCommand;
                try
                {
                    dataSet = new DataSet();
                    sqlAdapter.Fill(dataSet, "tempTable");
                }
                catch (SqlException exception)
                {
                    dataSet = null;
                    sqlConnection.Close();
                    sqlCommand.Dispose();
                    throw exception;
                }
                catch (Exception exception)
                {
                    dataSet = null;
                    sqlConnection.Close();
                    sqlCommand.Dispose();
                    throw exception;
                }

                sqlConnection.Close();
                sqlAdapter.Dispose();

            }

            return dataSet.Tables[0].CreateDataReader();

        }

        /// <summary>
        /// Executes a SQL statement with result like a Select command and returns a DataSet.
        /// </summary>
        /// <param name="dataSetName">Name of the data table to create.</param>
        /// <param name="sqlCommand">SQL command to be executed.</param>
        /// <returns>Returns a DataSet object with the data.</returns>
        public static DataSet FillDataSet(string dataSetName, SqlCommand sqlCommand,string dbName)
        {

            DataSet dataSet = null;

            using (SqlConnection sqlConnection = new SqlConnection(GetConnectionString(dbName)))
            {
                sqlConnection.Open();
                sqlCommand.Connection = sqlConnection;

                SqlDataAdapter sqlAdapter = new SqlDataAdapter();
                sqlAdapter.SelectCommand = sqlCommand;
                try
                {
                    dataSet = new DataSet();
                    sqlAdapter.Fill(dataSet, dataSetName);
                }
                catch (SqlException exception)
                {
                    dataSet = null;
                    sqlConnection.Close();
                    sqlCommand.Dispose();
                    throw exception;
                }
                catch (Exception exception)
                {
                    dataSet = null;
                    sqlConnection.Close();
                    sqlCommand.Dispose();
                    throw exception;
                }

                sqlConnection.Close();
                sqlAdapter.Dispose();

            }

            return dataSet;
        }

        /// <summary>
        /// Executes a SQL statement returning a single value.
        /// </summary>
        /// <param name="sqlCommand">SQL command to be executed.</param>
        /// <returns>Returns an object with the data.</returns>
        public static object ExecuteScalar(SqlCommand sqlCommand, string dbName)
        {
            object value;

            using (SqlConnection sqlConnection = new SqlConnection(GetConnectionString(dbName)))
            {
                sqlConnection.Open();
                sqlCommand.Connection = sqlConnection;

                try
                {
                    value = sqlCommand.ExecuteScalar();
                    if (value == null)
                        value = "";
                }
                catch (SqlException exception)
                {
                    sqlConnection.Close();
                    sqlCommand.Dispose();
                    throw exception;
                }
                catch (Exception exception)
                {
                    sqlConnection.Close();
                    sqlCommand.Dispose();
                    throw exception;
                }

                sqlConnection.Close();
                sqlCommand.Dispose();

            }

            return value;
        }

        #endregion
    }
}
