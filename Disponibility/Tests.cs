﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebApp.Models;
using WebApp.Controllers;
using System.Security.Cryptography.X509Certificates;
using System.Linq;

namespace Tests
{
    [TestClass]
    public class Tests
    {
        string DBName = "ActusBI_Oxxo";

        //TODO: put week in parameters to get current week from sp

        [TestMethod]
        public void MinProduction()
        {
            var Controller = new Controller();

            List<DisponibilityWeekly> DisponibilityList = Controller.GetDisponibility(DBName);


            var counter = 0;

            //MinProduction
            foreach (var DisponibilityRow in DisponibilityList)
            {
                if (DisponibilityRow.Production <= 0 || DisponibilityRow.RealDisplay <= 0)
                {
                    counter++;
                }
            }

            //foreach (var DisponibilityRow in DisponibilityList)
            //{
            //    if (DisponibilityRow.OnlineAverage <= 0)
            //    {
            //        counter++;
            //    }
            //}

            Assert.IsTrue(counter == 0, counter + " Tiendas tienen producción en cero.") ;
        }
        
        [TestMethod]
        public void OnlineAvg()
        {
            var Controller = new Controller();

            List<DisponibilityWeekly> DisponibilityList = Controller.GetDisponibility(DBName);

            var counter = 0;

            //MinProduction
            foreach (var DisponibilityRow in DisponibilityList)
            {
                if (DisponibilityRow.OnlineAverage <= 0)
                {
                    counter++;
                }
            }

            Assert.IsTrue(counter == 0, counter + " Tiendas tienen porcentaje de conectividad en cero.");
        }

        [TestMethod]
        public void RealProduccionNotZero()//produccion real en 0 con porcentaje de confiabilidad != 0
        {
            var Controller = new Controller();

            List<DisponibilityWeekly> DisponibilityList = Controller.GetDisponibility(DBName);


            var counter = 0;

            //MinProduction
            foreach (var DisponibilityRow in DisponibilityList)
            {
                if (DisponibilityRow.OnlineAverage !=0 && DisponibilityRow.RealDisplay == 0)
                {
                    counter++;
                }
            }

            Assert.IsTrue(counter == 0, counter + " Tiendas, tienen produccion en 0 con estatus en línea.");
        }
    }

}
