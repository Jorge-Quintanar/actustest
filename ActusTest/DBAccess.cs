﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActusTest
{
   
        class DBAccess
        {

            private static SqlConnection connection = new SqlConnection();
            private static SqlCommand command = new SqlCommand();
            private static SqlDataReader DbReader;
            private static SqlDataAdapter adapter = new SqlDataAdapter();
            public SqlTransaction DbTran;

            private static string ConnectionString = "Data Source=(local);Initial Catalog=SocialNetwork; Integrated Security=True";



            public void CreateConn()
            {
                try
                {
                    if (connection.State != ConnectionState.Open)
                    {
                        connection.ConnectionString = ConnectionString;
                        connection.Open();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }


            public void closeConn()
            {
                connection.Close();
            }


            public int ExecuteDataAdapter(DataTable tblName, string strSelectSql)
            {
                try
                {
                    if (connection.State == 0)
                    {
                        CreateConn();
                    }

                    adapter.SelectCommand.CommandText = strSelectSql;
                    adapter.SelectCommand.CommandType = CommandType.Text;
                    SqlCommandBuilder DbCommandBuilder = new SqlCommandBuilder(adapter);


                    string insert = DbCommandBuilder.GetInsertCommand().CommandText.ToString();
                    string update = DbCommandBuilder.GetUpdateCommand().CommandText.ToString();
                    string delete = DbCommandBuilder.GetDeleteCommand().CommandText.ToString();


                    return adapter.Update(tblName);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }


            public void ReadDatathroughAdapter(string query, DataTable tblName)
            {
                try
                {
                    if (connection.State == ConnectionState.Closed)
                    {
                        CreateConn();
                    }

                    command.Connection = connection;
                    command.CommandText = query;
                    command.CommandType = CommandType.Text;

                    adapter = new SqlDataAdapter(command);
                    adapter.Fill(tblName);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }


            public SqlDataReader ReadDatathroughReader(string query)
            {
                //DataReader used to sequentially read data from a data source
                SqlDataReader reader;

                try
                {
                    if (connection.State == ConnectionState.Closed)
                    {
                        CreateConn();
                    }

                    command.Connection = connection;
                    command.CommandText = query;
                    command.CommandType = CommandType.Text;

                    reader = command.ExecuteReader();
                    return reader;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }


            public int ExecuteQuery(SqlCommand dbCommand)
            {
                try
                {
                    if (connection.State == 0)
                    {
                        CreateConn();
                    }

                    dbCommand.Connection = connection;
                    dbCommand.CommandType = CommandType.Text;


                    return dbCommand.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
    }


