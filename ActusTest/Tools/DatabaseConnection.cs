﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace ActusTest.Tools
{
    /// <summary>
    /// Provides access to SQL Server database.
    /// </summary>
    public class DatabaseConnection
    {
        #region "Static Methods"

        /// <summary>
        /// Gets the connection string of the default database configured in Web.Config.
        /// </summary>
        /// <returns>Returns the connection string.</returns>
        public static string GetConnectionString()
        {
            string serverName = ConfigurationManager.AppSettings["ServerName"];
            string databaseName = ConfigurationManager.AppSettings["DatabaseName"];
            string databaseUser = ConfigurationManager.AppSettings["DatabaseUser"];
            string databasePassword = ConfigurationManager.AppSettings["DatabasePassword"];

            return "Data Source=" + serverName + ";User ID=" + databaseUser + ";Password=" + databasePassword + ";Initial Catalog=" + databaseName;
        }

        /// <summary>
        /// Executes a SQL statement with no result like the Insert, Delete or Update commands.
        /// </summary>
        /// <param name="sqlCommand">SQL command to be executed.</param>
        /// <returns>Returns the number of rows affected.</returns>
        public static int ExecuteNonQuery(SqlCommand sqlCommand)
        {
            int rowsAffected = 0;

            using (SqlConnection sqlConnection = new SqlConnection(GetConnectionString()))
            {
                sqlConnection.Open();
                sqlCommand.Connection = sqlConnection;
                try
                {
                    rowsAffected = sqlCommand.ExecuteNonQuery();
                }
                catch (SqlException exception)
                {
                    sqlConnection.Close();
                    sqlCommand.Dispose();
                    throw exception;
                }
                catch (Exception exception)
                {
                    sqlConnection.Close();
                    sqlCommand.Dispose();
                    throw exception;
                }

                sqlConnection.Close();
                sqlCommand.Dispose();

            }

            return rowsAffected;
        }

        /// <summary>
        /// Executes a SQL statement with result like a Select command.
        /// </summary>
        /// <param name="sqlCommand">SQL command to be executed.</param>
        /// <returns>Returns a DataTableReader object with the data.</returns>

        public static DataTableReader ExecuteQuery(SqlCommand sqlCommand)
        {

            DataSet dataSet = null;

            using (SqlConnection sqlConnection = new SqlConnection(GetConnectionString()))
            {
                sqlConnection.Open();
                sqlCommand.Connection = sqlConnection;

                SqlDataAdapter sqlAdapter = new SqlDataAdapter();
                sqlAdapter.SelectCommand = sqlCommand;
                try
                {
                    dataSet = new DataSet();
                    sqlAdapter.Fill(dataSet, "tempTable");
                }
                catch (SqlException exception)
                {
                    dataSet = null;
                    sqlConnection.Close();
                    sqlCommand.Dispose();
                    throw exception;
                }
                catch (Exception exception)
                {
                    dataSet = null;
                    sqlConnection.Close();
                    sqlCommand.Dispose();
                    throw exception;
                }

                sqlConnection.Close();
                sqlAdapter.Dispose();

            }

            return dataSet.Tables[0].CreateDataReader();

        }

        /// <summary>
        /// Executes a SQL statement with result like a Select command and returns a DataSet.
        /// </summary>
        /// <param name="dataSetName">Name of the data table to create.</param>
        /// <param name="sqlCommand">SQL command to be executed.</param>
        /// <returns>Returns a DataSet object with the data.</returns>
        public static DataSet FillDataSet(string dataSetName, SqlCommand sqlCommand)
        {

            DataSet dataSet = null;

            using (SqlConnection sqlConnection = new SqlConnection(GetConnectionString()))
            {
                sqlConnection.Open();
                sqlCommand.Connection = sqlConnection;

                SqlDataAdapter sqlAdapter = new SqlDataAdapter();
                sqlAdapter.SelectCommand = sqlCommand;
                try
                {
                    dataSet = new DataSet();
                    sqlAdapter.Fill(dataSet, dataSetName);
                }
                catch (SqlException exception)
                {
                    dataSet = null;
                    sqlConnection.Close();
                    sqlCommand.Dispose();
                    throw exception;
                }
                catch (Exception exception)
                {
                    dataSet = null;
                    sqlConnection.Close();
                    sqlCommand.Dispose();
                    throw exception;
                }

                sqlConnection.Close();
                sqlAdapter.Dispose();

            }

            return dataSet;
        }

        /// <summary>
        /// Executes a SQL statement returning a single value.
        /// </summary>
        /// <param name="sqlCommand">SQL command to be executed.</param>
        /// <returns>Returns an object with the data.</returns>
        public static object ExecuteScalar(SqlCommand sqlCommand)
        {
            object value;

            using (SqlConnection sqlConnection = new SqlConnection(GetConnectionString()))
            {
                sqlConnection.Open();
                sqlCommand.Connection = sqlConnection;

                try
                {
                    value = sqlCommand.ExecuteScalar();
                    if (value == null)
                        value = "";
                }
                catch (SqlException exception)
                {
                    sqlConnection.Close();
                    sqlCommand.Dispose();
                    throw exception;
                }
                catch (Exception exception)
                {
                    sqlConnection.Close();
                    sqlCommand.Dispose();
                    throw exception;
                }

                sqlConnection.Close();
                sqlCommand.Dispose();

            }

            return value;
        }

        #endregion
    }
}
