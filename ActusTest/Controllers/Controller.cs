﻿using ActusTest.Models;
using ActusTest.Tools;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.Web.Http;
using System.Web.Mvc;

namespace ActusTest.Controllers
{
    class Controller
    {

        [HttpGet]
        public List<Locations> LocationStart()
        {
            //if (Session["userid"] == null)
            //{
            //    return RedirectToAction("logout", "account");
            //}
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandText = "GetLocations";

            DataTableReader reader = DatabaseConnection.ExecuteQuery(sqlCommand);

            var LocationList = new List<Locations>();
            Locations OpeningCoffeRow;
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    OpeningCoffeRow = new Locations();
                    OpeningCoffeRow.Id = Convert.ToInt32(reader["Id"]);
                    OpeningCoffeRow.Displayname = reader["DisplayName"].ToString();
                    //Disponibility.PlazaName = reader["PlazaName"].ToString();
                    //Disponibility.Status = "Fuera de línea";
                    //if (Convert.ToBoolean(reader["OnlineStatus"]))
                    //{
                    //    Disponibility.Status = "En línea";
                    //}
                    //Disponibility.IsActive = AdminMaintenanceCatalogsResources.inActive;

                    //Disponibility.City = reader["City"].ToString();
                    LocationList.Add(OpeningCoffeRow);
                }
                reader.Read();
            }

            reader.Close();
            reader.Dispose();

            sqlCommand.Connection.Close();
            sqlCommand.Dispose();


            return LocationList;
        }
    }
}
